//
// Created by Nilupul Sandeepa on 2021-05-29.
//

#ifndef BRUSH_H
#define BRUSH_H

#include <string.h>
#include "../utils/Macros.h"
#include "../base/Drawable.h"
#include "../utils/Touch.h"
#include "../../../../../libs/openFrameworks/gl/ofFbo.h"
#include "../../../../../libs/openFrameworks/graphics/ofPolyline.h"
#include "../../../../../libs/openFrameworks/types/ofColor.h"
#include "../../../../../libs/openFrameworks/types/ofPoint.h"
#include "../../../../../libs/openFrameworks/math/ofVec3f.h"

NS_ADE_BEGIN

class Brush : public Drawable {
    public:
        Brush();

        virtual void setup();
        virtual void update();
        virtual void draw();
        virtual void drawUI();

        virtual void addPoint(const Touch &point, EventState state);
        virtual void clear();
        virtual void resetState();
        virtual void reload();
        virtual void setScale(float scale);
        virtual void setFbo(ofFbo* fbo);
        virtual void setStrokeWidth(float width);
        virtual void setBrushColor(float r, float g, float b);
        virtual void setRandomBrushColor(bool randomColor);
        virtual void setBrushImage(std::string file);
        virtual void setBrushOpacity(float opacity);
        virtual void showBrushShape();
        virtual void endStroke();
        virtual bool isDirty();
        ofTexture& getTexture();
        virtual void setSmudgeLength(float length);
        virtual void getBrushType(BrushType type);

        void lockColorChange(bool locked);

    protected:
        ofFbo* frameBuffer;
        ofPolyline strokeLine;
        ofPolyline forceLine;
        Touch touchPoints[3];
        int pointIndex;
        Touch lastPoint;
        Touch bezierPoint;
        float lastAngle;
        float scale;
        float strokeWidth;
        float smudgeLength;
        bool isDrawing;
        bool shouldShowBrushShape;
        ofFloatColor brushColor;

        ofPoint toXYPoint(const Touch &point) const {
            return ofPoint(ofVec3f(point.x, point.y, point.speed));
        }

        ofPoint toForcePoint(const Touch &point) const {
            return ofPoint(ofVec2f(point.force, point.angle));
        }
};

NS_ADE_END

#endif //BRUSH_H
