#include "EngineApp.h"
#include "../../../../libs/openFrameworks/utils/ofLog.h"

NS_ADE_BEGIN

EngineApp::EngineApp() {
	this->canvas = NULL;
}

//--------------------------------------------------------------
void EngineApp::setup() {

}

//--------------------------------------------------------------
void EngineApp::update() {

}

//--------------------------------------------------------------
void EngineApp::draw() {
    if (this->canvas != NULL) {
        this->canvas->draw();
    } else {
        ofClear(255, 255, 255, 255);
    }
}

//--------------------------------------------------------------
void EngineApp::keyPressed(int key) {
	
}

//--------------------------------------------------------------
void EngineApp::keyReleased(int key) {
	
}

//--------------------------------------------------------------
void EngineApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void EngineApp::touchDown(int x, int y, int id) {

}

//--------------------------------------------------------------
void EngineApp::touchMoved(int x, int y, int id) {

}

//--------------------------------------------------------------
void EngineApp::touchUp(int x, int y, int id) {

}

//--------------------------------------------------------------
void EngineApp::touchDoubleTap(int x, int y, int id) {

}

//--------------------------------------------------------------
void EngineApp::touchCancelled(int x, int y, int id) {

}

//--------------------------------------------------------------
void EngineApp::swipe(ofxAndroidSwipeDir swipeDir, int id) {

}

//--------------------------------------------------------------
void EngineApp::pause() {

}

//--------------------------------------------------------------
void EngineApp::stop() {

}

//--------------------------------------------------------------
void EngineApp::resume() {

}

//--------------------------------------------------------------
void EngineApp::reloadTextures() {

}

//--------------------------------------------------------------
bool EngineApp::backPressed() {
	return false;
}

//--------------------------------------------------------------
void EngineApp::okPressed() {

}

//--------------------------------------------------------------
void EngineApp::cancelPressed() {

}

Canvas* EngineApp::getCanvas() {
    return this->canvas;
}

void EngineApp::setupCanvas(int width, int height) {
    this->canvas = new Canvas(width, height);
    this->canvas->setup();
}

void EngineApp::setCanvasCallback(ade::ChangeCallback callback) {
	this->canvas->setCanvasCallback(callback);
}

NS_ADE_END