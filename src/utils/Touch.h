//
// Created by Nilupul Sandeepa on 2021-05-28.
//

#ifndef TOUCH_H
#define TOUCH_H

#include "Macros.h"
#include "../../../../../../android-ndk-r15c/sources/cxx-stl/llvm-libc++/include/cmath"

NS_ADE_BEGIN

struct Touch {

    float x, y;
    float speed;
    float force;
    float angle;

    Touch() {
        this->x = 0;
        this->y = 0;
        this->speed = 0;
        this->force = 1;
        this->angle = 0;
    }

    Touch(float xx, float yy) {
        this->x = xx;
        this->y = yy;
        this->speed = 0;
        this->force = 1;
        this->angle = 0;
    }

    Touch(float xx, float yy, float frc, float ang) {
        this->x = xx;
        this->y = yy;
        this->speed = 0;
        this->force = frc;
        this->angle = ang;
    }

    Touch(float xx, float yy, float spd, float frc, float ang) {
        this->x = xx;
        this->y = yy;
        this->speed = spd;
        this->force = frc;
        this->angle = ang;
    }

    inline Touch& setXY(float xx, float yy){
        this->x = xx;
        this->y = yy;
        return *this;
    }
    inline float distance(const Touch& pnt) const {
        float vx = this->x - pnt.x;
        float vy = this->y - pnt.y;
        return (float) sqrt((vx * vx) + (vy * vy));
    }

    inline float length() const {
        return (float) sqrt((this->x * this->x) + (this->y * this->y));
    }

    inline float angleWith(const Touch& vec) const {
        Touch n1 = this->getNormalized();
        Touch n2 = vec.getNormalized();
        return (float) (acos(n1.dot(n2)) * RADIAN_TO_DEGREE);
    }
    inline Touch getNormalized() const {
        float length = (float) sqrt((this->x * this->x) + (this->y * this->y));
        if(length > 0) {
            return Touch(this->x / length, this->y / length);
        } else {
            return Touch();
        }
    }
    inline float dot(const Touch& vec) const {
        return (this->x * vec.x) + (this->y * vec.y);
    }

    //operator overloading for Touch
    inline Touch operator + (const Touch& pnt) const {
        return Touch(this->x + pnt.x, this->y + pnt.y, this->speed + pnt.speed, this->force + pnt.force, this->angle + pnt.angle);
    }

    inline Touch& operator += (const Touch& pnt) {
        this->x += pnt.x;
        this->y += pnt.y;
        this->speed += pnt.speed;
        this->force += pnt.force;
        this->angle += pnt.angle;
        return *this;
    }

    inline Touch operator - (const Touch& pnt) const {
        return Touch(this->x - pnt.x, this->y - pnt.y, this->speed - pnt.speed, this->force - pnt.force, this->angle - pnt.angle);
    }

    inline Touch& operator -= (const Touch& pnt) {
        this->x -= pnt.x;
        this->y -= pnt.y;
        this->speed -= pnt.speed;
        this->force -= pnt.force;
        this->angle -= pnt.angle;
        return *this;
    }

    inline Touch operator * (const Touch& pnt) const {
        return Touch(this->x * pnt.x, this->y * pnt.y, this->speed * pnt.speed, this->force * pnt.force, this->angle * pnt.angle);
    }

    inline Touch& operator *= ( const Touch& pnt) {
        this->x *= pnt.x;
        this->y *= pnt.y;
        this->speed *= pnt.speed;
        this->force *= pnt.force;
        this->angle *= pnt.angle;
        return *this;
    }

    inline Touch operator / (const Touch& pnt) const {
        return Touch(pnt.x != 0 ? this->x / pnt.x : this->x, pnt.y != 0 ? this->y / pnt.y : this->y, pnt.speed != 0 ? this->speed / pnt.speed : this->speed, pnt.force != 0 ? this->force / pnt.force : this->force, pnt.angle != 0 ? this->angle / pnt.angle : this->angle);
    }

    inline Touch& operator /= (const Touch& pnt) {
        this->x = pnt.x != 0 ? this->x / pnt.x : this->x;
        this->y = pnt.y != 0 ? this->y / pnt.y : this->y;
        this->speed = pnt.speed != 0 ? this->speed / pnt.speed : this->speed;
        this->force = pnt.force != 0 ? this->force / pnt.force : this->force;
        this->angle = pnt.angle != 0 ? this->angle / pnt.angle : this->angle;
        return *this;
    }

    inline Touch operator - () const {
        return Touch(-x, -y, -speed, -force, -angle);
    }

    //operator overloading for float
    inline Touch operator * (const float f) const {
        return Touch(this->x * f, this->y * f, this->speed * f, this->force * f, this->angle * f);
    }

    inline Touch& operator *= (const float f) {
        this->x *= f;
        this->y *= f;
        this->speed *= f;
        this->force *= f;
        this->angle *= f;
        return *this;
    }

    inline Touch operator / (const float f) const {
        if(f == 0) {
            return Touch(this->x, this->y, this->speed, this->force, this->angle);
        }
        return Touch(this->x / f, this->y / f, this->speed / f, this->force / f, this->angle / f);
    }

    inline Touch& operator /= (const float f) {
        if(f == 0) {
            return *this;
        }
        this->x /= f;
        this->y /= f;
        this->speed /= f;
        this->force /= f;
        this->angle /= f;
        return *this;
    }
};

Touch operator * (float f, const Touch& t);
inline Touch operator * (float f, const Touch& t) {
    return Touch(t.x * f, t.y * f, t.speed * f, t.force * f, t.angle * f);
}

NS_ADE_END

#endif //TOUCH_H