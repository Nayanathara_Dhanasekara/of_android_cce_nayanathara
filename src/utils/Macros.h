//
// Created by Nilupul Sandeepa on 2021-05-28.
//

#ifndef MACROS_H
#define MACROS_H

#define NS_ADE_BEGIN                     namespace ade {
#define NS_ADE_END                       }
#define USING_NS_ADE                     using namespace ade;
#define NS_ADE                           ::ade

#define RADIAN_TO_DEGREE 57.2957795
#define DEGREE_TO_RADIAN 0.01745329

#endif //MACROS_H